package mahlzeit;
public class Mahlzeit {
    
    private static String name ;
    private double preis;
    private static String beschreibung;
    private static boolean isVegetarisch = false;

     Mahlzeit(String name , double preis , boolean isVegetarisch, String beschreibung) {
        this.name = name ;
        this.preis = preis;
        this.beschreibung = beschreibung;
        this.isVegetarisch = isVegetarisch;
    }
      
     Mahlzeit(String name , boolean isVegetarisch , String beschreibung) {
       
        this(name ,2.50 , isVegetarisch, beschreibung);
        
    }

    public double gibStudentPreis() {
         return preis;
    }

    public double gibMitarbeiterPreis() {
        return preis + 2;
   }

   public double gibGastPreis() {
    return preis + 2.5;
}
    

    public String toString() {
        return "Name :" + name + " Preis : " + gibStudentPreis() + " / " +gibMitarbeiterPreis() + " / " + gibGastPreis()  + " Beschreibung : " + beschreibung + " Ist Vegetarisch : " + isVegetarisch;
    }
}
